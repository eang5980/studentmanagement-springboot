package com.dxc.StudentManagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dxc.StudentManagement.entity.Student;

public interface StudentRepository extends JpaRepository<Student,Long >{
	
}
